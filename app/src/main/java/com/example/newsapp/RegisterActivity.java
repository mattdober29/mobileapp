package com.example.newsapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.provider.Telephony.Mms.Part.TEXT;

public class RegisterActivity extends AppCompatActivity {

    boolean memberMatched = false;

    EditText editTextName, editTextEmail, editTextPassword, editTextNotifications;
    Button btnRegister;

    DatabaseReference databaseMembers;
    RecyclerView recyclerViewMembers;
    List<Member> memberList;


    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String EMAIL = "email";
    public static final String NAME = "nameINPref";
    public static final String PASSWORD = "password";
    public static final String TIME = "time";

    private String email, nameINPref, password, time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        memberMatched = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");
        editTextName = findViewById(R.id.txtName);
        editTextEmail = findViewById(R.id.txtEmail);
        editTextPassword = findViewById(R.id.txtPassword);
        editTextNotifications = findViewById(R.id.editTextNotification);
        btnRegister = findViewById(R.id.btnRegister);


        memberList = new ArrayList<>();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                //SharedPreferences.Editor editor = sharedPreferences.edit();

                if(memberList.size() < 1)
                {
                    addMember();
                }else{
                    for (Member member : memberList) {
                        if(member.getEmail().equals(editTextEmail.getText().toString().trim()))
                        {
                            Toast.makeText(RegisterActivity.this,"User exists please try to login" +
                                    " instead",Toast.LENGTH_LONG).show();
                            memberMatched = true;
                            GoTOLogin();
                        }
                    }
                    if(memberMatched == false)
                    {
                      addMember();
                    }else{

                    }
                }
            }
        });

    }

    private void addMember() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String name = editTextName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String notifications = editTextNotifications.getText().toString().trim();
        if (!TextUtils.isEmpty(name) && (!TextUtils.isEmpty(email)) && (!TextUtils.isEmpty(password))) {
            String id = databaseMembers.push().getKey();
            Member member = new Member(id, name, password, email, notifications);
            databaseMembers.child(id).setValue(member);
            Toast.makeText(this, "Member Added", Toast.LENGTH_LONG).show();
            editor.clear().apply();
            GoTOLogin();
        } else {
            Toast.makeText(this, "Please fill in all inputs", Toast.LENGTH_LONG).show();
        }


    }

    public void onStart() {
        super.onStart();
        loadData();
        updateViews();
        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");
        databaseMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot memberSnapshot : dataSnapshot.getChildren()) {

                    Member member = memberSnapshot.getValue(Member.class);
                    memberList.add(member);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void GoTOLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void onResume(){
        super.onResume();
        loadData();
        updateViews();


    }

    protected void onPause(){
        super.onPause();
        if(memberMatched == false)
        {
            saveData();
        }

    }

    protected  void onStop(){
        super.onStop();
        if(memberMatched == false)
        {
            saveData();
        }
    }

    public void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Calendar calendar = Calendar.getInstance();
        String currentCalendar = DateFormat.getDateTimeInstance().format(calendar.getTime());
        editor.putString(TIME,currentCalendar);
        editor.putString(EMAIL, editTextEmail.getText().toString());
        editor.putString(NAME, editTextName.getText().toString());
        editor.putString(PASSWORD, editTextPassword.getText().toString());

        editor.apply();


    }
    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        email = sharedPreferences.getString(EMAIL, "");
        nameINPref = sharedPreferences.getString(NAME, "");
        password = sharedPreferences.getString(PASSWORD, "");
        time = sharedPreferences.getString(TIME,"");

    }

    public void updateViews(){
        editTextEmail.setText(email);
        editTextName.setText(nameINPref);
        editTextPassword.setText(password);
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), time, Snackbar.LENGTH_LONG);
        snackbar.show();

    }

}
