package com.example.newsapp;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder> {

    private ArrayList<ExampleItem> mExampleList;

    public static class ExampleViewHolder extends RecyclerView.ViewHolder{
        public ImageView mimageView;
        public TextView mTextView1;
        public TextView mTextView2;
        public TextView mTextView3;
        public TextView mTextView4;
        public TextView mTextView5;
        public Button btnUrl;
        public ExampleViewHolder(final View itemView)
        {
            super(itemView);

            mTextView1 = itemView.findViewById(R.id.textView);
            mTextView2 = itemView.findViewById(R.id.textView2);
            mTextView3 = itemView.findViewById(R.id.textView3);
            mTextView4 = itemView.findViewById(R.id.textView4);
            mTextView5 = itemView.findViewById(R.id.textViewHidden);
            btnUrl = itemView.findViewById(R.id.urlBtn);


            btnUrl.setOnClickListener(new View.OnClickListener(){
                public void onClick(View view){
                    Toast.makeText(itemView.getContext(),"TEST",Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mTextView5.getText().toString()));
                    itemView.getContext().startActivity(intent);

                }
            });
        }


    }

    public ExampleAdapter(ArrayList<ExampleItem> exampleList){
        mExampleList = exampleList;
    }


    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.example_item, parent, false);
        ExampleViewHolder evh = new ExampleViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position)
    {
            ExampleItem currentItem = mExampleList.get(position);

            holder.mTextView1.setText(currentItem.getmText1());
            holder.mTextView2.setText(currentItem.getmText2());
            holder.mTextView3.setText(currentItem.getmText3());
            holder.mTextView4.setText(currentItem.getmText4());
            holder.mTextView5.setText(currentItem.getmText4());
    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}
