package com.example.newsapp;

public class NewsTemplate {
    String Title;
    String desc;
    String published;
    String url;
    String imageUrl;

    public NewsTemplate() {
    }

    public NewsTemplate(String title, String desc, String published, String url, String imageUrl) {
        this.Title = title;
        this.desc = desc;
        this.published = published;
        this.url = url;
        this.imageUrl = imageUrl;
    }



    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
