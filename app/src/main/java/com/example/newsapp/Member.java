package com.example.newsapp;

import java.io.Serializable;

public class Member implements Serializable {
    private String id;
    private String Name;
    private String Password;
    private String Email;
    private String Notify;

    public String getNotify() {
        return Notify;
    }

    public void setNotify(String notify) {
        this.Notify = notify;
    }

    public String getName() {
        return Name;
    }

    public Member(String id, String name, String password, String email, String notify) {
        this.id = id;
        Name = name;
        Password = password;
        Email = email;
        Notify = notify;
    }

    public Member() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return Password;
    }

    public String getEmail() {
        return Email;
    }


    public void setName(String name) {
        Name = name;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
