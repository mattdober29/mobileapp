package com.example.newsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class PinCode extends AppCompatActivity {

    Pinview pinview;
    String code = "12345";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_code);
        pinview = findViewById(R.id.mypinview);

        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                if(pinview.getValue().equals("12345"))
                {
                   RemovePin();
                }else{
                    Toast.makeText(getApplicationContext(), "Incorrect Pin",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public void RemovePin()
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }
}
