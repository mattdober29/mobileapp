

package com.example.newsapp;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class GetNews extends AsyncTask<String, Void, List<NewsTemplate>> {

    @Override
    protected List<NewsTemplate> doInBackground(String... urls) {
        List<NewsTemplate> newsTemp = new ArrayList<>();
        URL url;
        HttpsURLConnection urlConnection = null;
        String result = "";
        try {
            url = new URL(urls[0]);
            urlConnection = (HttpsURLConnection) url.openConnection();
            String response = streamToString(urlConnection.getInputStream());
            newsTemp=parseResult(response);
            return newsTemp;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    private String streamToString(InputStream stream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String data;
        String result = "";

        while((data = bufferedReader.readLine()) != null){
            result += data;
        }
        if(null != stream){
            stream.close();
        }
        return  result;
    }
    private  List<NewsTemplate> parseResult(String result){

        List<NewsTemplate> newsTemp = new ArrayList<>();
        JSONObject response = null;
        try {
            response = new JSONObject(result);
            JSONArray articles = response.optJSONArray("articles");

            for(int i = 0; i< articles.length(); i++)
            {
                JSONObject article = articles.optJSONObject(i);
                String title = article.getString("title");
                String desc = article.getString("description");
                String published = article.getString("publishedAt");
                String url = article.getString("url");
                String imageUrl = article.getString("urlToImage");
                newsTemp.add(new NewsTemplate(title,desc,published,url,imageUrl));
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

return newsTemp;
    }


}

