package com.example.newsapp;

import android.app.NotificationManager;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    boolean memberFound, passwordMatch = false;
    EditText editTextEmail, editTextPassword;
    Button btnLogin;
    DatabaseReference databaseMembers;
    List<Member>memberList;;
    String Email, Notify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editTextEmail = findViewById(R.id.txtEmail);
        editTextPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        memberList = new ArrayList<>();
        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                for(Member member : memberList)
                {
                    if(member.getEmail().equals(editTextEmail.getText().toString().trim())){
                        memberFound = true;
                        if(member.getPassword().equals(editTextPassword.getText().toString().trim()))
                        {
                            passwordMatch = true;
                            Email = member.getEmail();
                            Notify = member.getNotify();
                        }
                    }
                }
                if(memberFound == true && passwordMatch == true)
                {
                    login(Email);
                }else{
                    if(memberFound == true && !passwordMatch)
                    {
                        Toast.makeText(LoginActivity.this,"Invalid password",Toast.LENGTH_LONG).show();
                        memberFound = false;
                    }else{
                        Toast.makeText(LoginActivity.this,"User not found please register first",Toast.LENGTH_LONG).show();
                    }

                }

            }
        });
    }

    public void login(String Email){
        Intent intent = new Intent(this, NewsLayoutV2.class);
        intent.putExtra("email",Email);
        intent.putExtra("notify",Notify);
        startActivity(intent);
    }

    public void onStart() {
        super.onStart();
        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");
        databaseMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot memberSnapshot : dataSnapshot.getChildren()) {
                    Member member = memberSnapshot.getValue(Member.class);
                    memberList.add(member);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
