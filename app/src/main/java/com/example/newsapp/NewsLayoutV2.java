package com.example.newsapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class NewsLayoutV2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int unqiueID = 2108;
    public Button btnSearch,btnAdd,btnRemove;
    EditText searchTerm;
    DatabaseReference databaseMembers,databaseFavorites;
    Spinner spinner;
    List<NewsTemplate> newsTemp = new ArrayList<>();
    private RecyclerView mRecyclerView;
    public  static TextView data;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<Member> members = new ArrayList<>();
    List<String> favorites = new ArrayList<>();
    DatabaseReference databaseFavEmails;
    List<String> favoriteEmails = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_layout_v2);
        notificationSent();
        Intent intent = getIntent();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        spinner = findViewById(R.id.favSpinnter);
        databaseMembers = FirebaseDatabase.getInstance().getReference("Member");

        databaseFavorites = FirebaseDatabase.getInstance().getReference("Favorites");
        final String Email = intent.getStringExtra("email");
        String notify = intent.getStringExtra("notify");
//        if(notify.equals("Yes"))
//        {
//
////            private static final int uniqueID = 12398;
//        }
        getMemberData();
        Member member = getMemberObject(Email);
//        List<String> favorites = getFavorites(member.getEmail());
        searchTerm = findViewById(R.id.searchTxt);
        btnSearch = findViewById(R.id.searchBtn);
        btnAdd = findViewById(R.id.addFavBtn);

        btnRemove = findViewById(R.id.removeFavBtn);
        btnRemove.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view)
            {
               removeFavorite(Email);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view)
            {
               if(addFavoriteEmail().contains(Email) == false)
               {
                   databaseFavorites.push().setValue(Email);
               }
               databaseFavEmails = databaseFavorites.child(Email);
               if(!checkIfFavExisits().contains(searchTerm.getText().toString()))
                {
                    databaseFavEmails.push().setValue(searchTerm.getText().toString());
                }
            }

            private List<String> checkIfFavExisits() {
                final List<String> favorites = new ArrayList<>();
                databaseFavEmails.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot favoriteEmailSnap : dataSnapshot.getChildren()){
                            favorites.add(favoriteEmailSnap.getValue().toString());
                        }

                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                return favorites;
            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view)
            {
                String searchInput = searchTerm.getText().toString().trim();
                searchInput = searchInput.replace(" ","%20");
                GetNews client = new GetNews();
                try {
                    newsTemp = client.execute("https://newsapi.org/v2/everything?q="+searchInput+"&sortBy=publishedAt&apiKey=1412799789d84778bcdc8e8ea1e3d8ca").get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ArrayList<ExampleItem> exampleItem = new ArrayList<>();
                for(NewsTemplate newsTemplate : newsTemp)
                {
                    exampleItem.add(new ExampleItem(newsTemplate.Title,newsTemplate.desc,newsTemplate.published,newsTemplate.url,newsTemplate.imageUrl));
                }

                mRecyclerView = findViewById(R.id.recyclerView);
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(NewsLayoutV2.this);
                mAdapter = new ExampleAdapter(exampleItem);

                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.setAdapter(mAdapter);

            }
        });
    }

    public void notificationSent()

    {

        NotificationCompat.Builder notification;
        notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Notification requested new newss avaliable")
                .setContentText("Awesome notification working!")
                .setPriority(Notification.PRIORITY_MAX);
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


        nm.notify(unqiueID,notification.build());
    }

    private void removeFavorite(String email) {
        DatabaseReference emailToRemoveFrom = FirebaseDatabase.getInstance().getReference("Favorites").child(email);
        DatabaseReference newsToRemove = emailToRemoveFrom.child(searchTerm.getText().toString().trim());

        newsToRemove.removeValue();


    }

    private List<String> getFavorites(String email) {
        return null;
    }

    public List<String> addFavoriteEmail(){


        databaseFavorites.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               for(DataSnapshot favoriteEmailSnap : dataSnapshot.getChildren()){
                   favoriteEmails.add(favoriteEmailSnap.getValue().toString());
               }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
//        databaseFavorites.push().setValue("hello");
        return favoriteEmails;
    }

    private Member getMemberObject(String email) {
        for(Member member : members)
        {
            if(member.getEmail().equals(email))
            {
                return member;
            }
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.news_layout_v2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logOut) {
            logOut();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.logOut) {
            logOut();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logOut() {
        Intent intent = new Intent(this,MainMenuActivity.class);
        startActivity(intent);
    }

    public void addFavorites(){
//        if(!emailIsSet)
//        {
//            databaseFavorites.push().setValue(email);
//        }
//
//                databaseFavEmails.push().setValue(searchTerm.getText().toString());
    }

    public void getFavoritesDataByEmail()
    {



        databaseFavorites.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot favoritesSnapshot : dataSnapshot.getChildren()) {

                    String favorite = favoritesSnapshot.getValue().toString();
                    favorites.add(favorite);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getMemberData()
    {


        databaseMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot memberSnapshot : dataSnapshot.getChildren()) {

                    Member member = memberSnapshot.getValue(Member.class);
                    members.add(member);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
